<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>第五回課題　配列</title>
    </head>
    <body>
        <font size="5" clolr="blue" face="MS ゴシック">
            <center>
                <h1>第五回課題　連想配列</h1>
                <?php
                $retu_data = [
                    'shikaku'=>'JSTQB'.'<br>',
                    'birthday'=>'0309'.'<br>',
                    'wr'=>'バグを憎んで人を憎まず'.'<br>',
                    'like'=>'料理'.'<br>',
                    'game'=>'ロールプレイング'.'<br>',
                    'wakusei'=>'地球'.'<br>',
                    'pokemonn'=>'ピカチュウ'.'<br>',
                ];
                echo $retu_data['shikaku'];
                echo $retu_data['birthday'];
                echo $retu_data['wr'];
                echo $retu_data['like'];
                echo $retu_data['game'];
                echo $retu_data['wakusei'];
                echo $retu_data['pokemonn'];
                ?>
                <br>
                <hr>
                <pre>
                    <?php var_dump($retu_data); ?>
                </pre>
                <br>
                <hr>
                <?php
                $retu_data2 = array(
                    'name'=>'金子'.'<br>',
                    'syutoku'=>'基本情報技術者'.'<br>',
                    'syumi'=>'絵を描く'.'<br>',
                    'inu'=>'ミニチュアダックス'.'<br>',
                    'adana'=>'たまご'.'<br>',
                );
                echo $retu_data2['name'];
                echo $retu_data2['syutoku'];
                echo $retu_data2['syumi'];
                echo $retu_data2['inu'];
                echo $retu_data2['adana'];
                ?>
                <br>
                <hr>
                <pre>
                    <?php var_dump($retu_data2); ?>
                </pre>
                <br>
                <hr>
                <?php
                $retu_data3['sumaho'] = 'iphone'.'<br>';
                $retu_data3['sns'] = 'Twitter'.'<br>';
                $retu_data3['ken'] = '福島'.'<br>';
                $retu_data3['doubutu'] = '犬'.'<br>';
                $retu_data3['hosi'] = 'カービィ'.'<br>';
                echo $retu_data3['sumaho'];
                echo $retu_data3['sns'];
                echo $retu_data3['ken'];
                echo $retu_data3['doubutu'];
                echo $retu_data3['hosi'];
                ?>
                <br>
                <hr>
                <pre>
                    <?php var_dump($retu_data3); ?>
                </pre>
            </center>
        </font>
    </body>
</html>
