<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>第五回課題　配列</title>
    </head>
    <body>
        <font size="5" clolr="blue" face="MS ゴシック">
            <center>
                <h1>第五回課題　連想配列2</h1>
                <?php
                $shikaku = [
                    'shikaku'=>'JSTQB'.'<br>',
                    'birthday'=>'0309'.'<br>',
                    'wr'=>'バグを憎んで人を憎まず'.'<br>',
                    'like'=>'料理'.'<br>',
                    'game'=>'ロールプレイング'.'<br>',
                    'wakusei'=>'地球'.'<br>',
                    'pokemonn'=>'ピカチュウ'.'<br>',
                ];
                foreach ($shikaku as $namae) {
                    echo $namae;// code...
                }
                ?>
                <hr>
                <table border="2">
                    <?php
                    foreach ($shikaku as $number => $value) {
                        echo "<tr>";
                            echo "<td>".$number."</td><td>".$value."</td>";
                        echo "<tr>";
                    }
                ?>
                </table>
                <hr>
                <pre>
                    <?php
                    var_dump($shikaku);
                    ?>
                </pre>
            </center>
        </font>
    </body>
</html>
