<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>第五回課題　配列</title>
    </head>
    <body>
        <font size="5" clolr="blue" face="MS ゴシック">
            <center>
                <table border="3">
                    <h1>第五回課題　二次元配列3</h1>
                    <?php
                    $mahjong01 = [
                        "役満" => "国士無双",
                        "子" => "32000",
                        "親" => "48000",
                        "重複" => "一倍",
                        "鳴き" => "不可",
                    ];
                    $mahjong02 = [
                        "役満" => "大三元",
                        "子" => "32000",
                        "親" => "48000",
                        "重複" => "一倍",
                        "鳴き" => "可",
                    ];
                    $mahjong03 = [
                        "役満" => "純正九蓮宝燈",
                        "子" => "62000",
                        "親" => "93000",
                        "重複" => "二倍",
                        "鳴き" => "不可",
                    ];
                    $mahjong04 = [
                        "役満" => "小四喜",
                        "子" => "32000",
                        "親" => "48000",
                        "重複" => "一倍",
                        "鳴き" => "不可",
                    ];
                    $mahjong05 = [
                        "役満" => "八連荘:天和:字一色:大四喜:四暗刻単騎",
                        "子" => "224000",
                        "親" => "336000",
                        "重複" => "七倍",
                        "鳴き" => "不可",
                    ];
                    $mahjyong_yakuman = [$mahjong01,$mahjong02,$mahjong03,$mahjong04,$mahjong05];
                    echo "<td>"."役満名"."</td>".
                        "<td>"."子の場合"."</td>".
                        "<td>"."親の場合"."</td>".
                        "<td>"."何倍"."</td>".
                        "<td>"."鳴き"."</td>";
                    foreach ($mahjyong_yakuman as $value) {
                        echo "<tr>";
                            echo "<td>".$value["役満"]."</td>".
                                "<td>".$value["子"]."</td>".
                                "<td>".$value["親"]."</td>".
                                "<td>".$value["重複"]."</td>".
                                "<td>".$value["鳴き"]."</td>";
                        echo "</tr>";
                    }
                    ?>
                </table>
            </center>
        </font>
    </body>
</html>
