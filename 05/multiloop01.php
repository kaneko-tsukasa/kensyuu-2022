<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>第五回課題　配列</title>
    </head>
    <body>
        <font size="5" clolr="blue" face="MS ゴシック">
            <center>
                <h1>第五回課題　二次元配列</h1>
                <table border="3">
                    <h2>良い子の五十音表</h2>
                    <?php
                    $hiragana1 = ['あ','い','う','え','お','||','は','ひ','ふ','へ','ほ'];
                    $hiragana2 = ['か','き','く','け','こ','||','ま','み','む','め','も'];
                    $hiragana3 = ['さ','し','す','せ','そ','||','や','　','ゆ','　','よ'];
                    $hiragana4 = ['た','ち','つ','て','と','||','ら','り','る','れ','ろ'];
                    $hiragana5 = ['な','に','ぬ','ね','の','||','わ','　','を','　','ん'];
                    $hiraganahyou = [$hiragana1,$hiragana2,$hiragana3,$hiragana4,$hiragana5];
                    foreach ($hiraganahyou as $value) {
                        echo "<tr>";
                        foreach ($value as $number) {
                            echo "<td>".$number."</td>";// code...
                        }
                        echo"</tr>";
                    }
                    ?>
                </table>
                <?php
                echo "<pre>";
                var_dump($hiraganahyou);
                echo "</pre>";
                ?>
            </center>
        </font>
    </body>
</html>
