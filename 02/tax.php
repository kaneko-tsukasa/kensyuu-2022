
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>第二回課題、フォーム部品練習</title>
    </head>
    <body>
        <h1>第二回課題　フォーム部品練習</h1>
        <form method='POST' action='tax.php'>
            <table border="1">
                <tr>
                    <th>商品名</th>
                    <th>価格（単位:円・税抜き)</th>
                    <th>個数</th>
                    <th>税率</th>
                </tr>
                <tr>
                    <td><input type=text name="namae"></td>
                    <td><input type=text name="kakaku"></td>
                    <td><input type=text name="kosuu">個</td>
                    <td><input type=radio name="zei" value="1.08" checked>8%<input type=radio name="zei" value="1.1">10%</td>
                </tr>
                <tr>
                    <td><input type=text name="namae2"></td>
                    <td><input type=text name="kakaku2"></td>
                    <td><input type=text name="kosuu2">個</td>
                    <td><input type=radio name="zei2" value="1.08" checked>8%<input type=radio name="zei2" value="1.1">10%</td>
                </tr>
                <tr>
                    <td><input type=text name="namae3"></td>
                    <td><input type=text name="kakaku3"></td>
                    <td><input type=text name="kosuu3">個</td>
                    <td><input type=radio name="zei3" value="1.08" checked>8%<input type=radio name="zei3" value="1.1">10%</td>
                </tr>
                <tr>
                    <td><input type=text name="namae4"></td>
                    <td><input type=text name="kakaku4"></td>
                    <td><input type=text name="kosuu4">個</td>
                    <td><input type=radio name="zei4" value="1.08" checked>8%<input type=radio name="zei4" value="1.1">10%</td>
                </tr>
                <tr>
                    <td><input type=text name="namae5"></td>
                    <td><input type=text name="kakaku5"></td>
                    <td><input type=text name="kosuu5">個</td>
                    <td><input type=radio name="zei5" value="1.08" checked>8%<input type=radio name="zei5" value="1.1">10%</td>
                </tr>
            </table>
            <tr>
                <td><input type=submit value=" 送信 "><input type=reset value=" 取消 "></td>
            </tr>
        </form>
        <br>
        <?php
        $price = $_POST['kakaku'];
        $kazu = $_POST['kosuu'];
        $zeiritu = $_POST['zei'];
        $gokei = $price * $kazu;
        $zeikomi = $gokei * $zeiritu;
         ?>
         <?php
         $price2 = $_POST['kakaku2'];
         $kazu2 = $_POST['kosuu2'];
         $zeiritu2 = $_POST['zei2'];
         $gokei2 = $price2 * $kazu2;
         $zeikomi2 = $gokei2 * $zeiritu2;
          ?>
          <?php
          $price3 = $_POST['kakaku3'];
          $kazu3 = $_POST['kosuu3'];
          $zeiritu3 = $_POST['zei3'];
          $gokei3 = $price3 * $kazu3;
          $zeikomi3 = $gokei3 * $zeiritu3;
           ?>
           <?php
           $price4 = $_POST['kakaku4'];
           $kazu4 = $_POST['kosuu4'];
           $zeiritu4 = $_POST['zei4'];
           $gokei4 = $price4 * $kazu4;
           $zeikomi4 = $gokei4 * $zeiritu4;
            ?>
            <?php
            $price5 = $_POST['kakaku5'];
            $kazu5 = $_POST['kosuu5'];
            $zeiritu5 = $_POST['zei5'];
            $gokei5 = $price5 * $kazu5;
            $zeikomi5 = $gokei5 * $zeiritu5;
             ?>
        <table border="1" >
            <tr>
                <th>商品名</th>
                <th>価格（単位:円・税抜き)</th>
                <th>個数</th>
                <th>税率</th>
                <th>小計(単位：円)</th>
            </tr>

            <tr>
                <td><?php echo $_POST['namae'];?></td>
                <td><?php echo $_POST['kakaku'];?></td>
                <td><?php echo $_POST['kosuu'];?></td>
                <td><?php echo $_POST['zei'];?></td>
                <td><?php echo $zeikomi;?></td>
            </tr>
            <tr>
                <td><?php echo $_POST['namae2'];?></td>
                <td><?php echo $_POST['kakaku2'];?></td>
                <td><?php echo $_POST['kosuu2'];?></td>
                <td><?php echo $_POST['zei2'];?></td>
                <td><?php echo $zeikomi2;?></td>
            </tr>
            <tr>
                <td><?php echo $_POST['namae3'];?></td>
                <td><?php echo $_POST['kakaku3'];?></td>
                <td><?php echo $_POST['kosuu3'];?></td>
                <td><?php echo $_POST['zei3'];?></td>
                <td><?php echo $zeikomi3;?></td>
            </tr>
            <tr>
                <td><?php echo $_POST['namae4'];?></td>
                <td><?php echo $_POST['kakaku4'];?></td>
                <td><?php echo $_POST['kosuu4'];?></td>
                <td><?php echo $_POST['zei4'];?></td>
                <td><?php echo $zeikomi4;?></td>
            </tr>
            <tr>
                <td><?php echo $_POST['namae5'];?></td>
                <td><?php echo $_POST['kakaku5'];?></td>
                <td><?php echo $_POST['kosuu5'];?></td>
                <td><?php echo $_POST['zei5'];?></td>
                <td><?php echo $zeikomi5;?></td>
            </tr>
            <tr>
                <th>合計</th>
                <?php
                $all = $zeikomi + $zeikomi2;
                $all = $all + $zeikomi3;
                $all = $all + $zeikomi4;
                $all = $all + $zeikomi5;
                ?>
                <td><?php echo $all;?></td>
            </tr>
        </table>
    </body>
</html>
