<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>第五回課題　配列</title>
    </head>
    <body>
        <font size="5" clolr="blue" face="MS ゴシック">
            <center>
                <h1>第五回課題　配列2</h1>
                <?php
                $shikaku = ["JSTQB","ISTQB","IVEC","JCSQE","ITパスポート","基本情報技術者","応用情報技術者"];
                $shikaku2 = array("応用","基本","Iパス","JCSQE","IVEC","ISTQB","JSTQB");
                $wakusei[1] ="水星";
                $wakusei[2] ="金星";
                $wakusei[3] ="地球";
                $wakusei[4] ="火星";
                $wakusei[5] ="木星";
                $wakusei[6] ="土星";
                $wakusei[7] ="天王星";

                echo "<br>";
                for ($number=0; $number < count($shikaku); $number++){
                    echo "<br>$shikaku[$number]";// code...
                }
                echo "<br>";
                ?>
                <pre>
                <?php
                var_dump($shikaku);
                ?>
                </pre>
                <br>
                <?php
                echo "<br>";
                foreach ($shikaku2 as $namae) {
                    echo "<br>$namae";// code...
                }
                echo "<br>";
                ?>
                <pre>
                <?php
                var_dump($shikaku2);
                ?>
                </pre>
                <br>
                <?php
                echo "<br>";
                echo "<br>";
                foreach ($wakusei as $jyun => $value) {
                    echo $jyun."番の惑星は".$value."です。<br>";// code...
                }
                ?>
                <pre>
                <?php
                var_dump($wakusei);
                ?>
                </pre>
                <br>
                <h1>以下　配列の検索</h1>
                <?php
                $kesiki = ["滝","星","空","夜空","海","川","山"];
                echo "<br>";
                echo "<br>";
                $basyo = "星";
                if (in_array($basyo,$kesiki)) {
                    echo $basyo."がkesiki配列内に見つかりました";
                }else {
                    echo $basyo."がkesiki配列内に見つかりませんでした";
                }
                ?>
                <pre>
                <?php
                var_dump($kesiki);
                ?>
                </pre>
            </font>
        </center>
    </body>
</html>
