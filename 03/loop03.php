<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>第四回課題、ループ</title>
    </head>
    <body>
        <h1>第四回課題、座標を入れる</h1>
        <form method='GET' action='loop03.php'>
            <input type="number" name="loop">行<br><input type="number" name="loop2">列
            <br>
            <input type=submit value=" 作成 "><input type=reset value=" 取消 ">
        </form>
        <table border="1">
            <?php
            for($retu=1; $retu <= $_GET['loop2']; $retu++){
                echo "<tr>";
                for ($gyou=1; $gyou <= $_GET['loop']; $gyou++){
                    echo "<td>$retu-$gyou</td>";
                }
                echo "</tr>";
            }
            ?>
        </table>
    </body>
</html>
