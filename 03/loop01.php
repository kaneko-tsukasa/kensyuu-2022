<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>第四回課題、ループ</title>
    </head>
    <body>
        <form method='GET' action='loop01.php'>
            <input type="number" name="loop">行で作成
            <br>
            <input type=submit value=" 作成 "><input type=reset value=" 取消 ">
        </form>
        <table border="1">
            <?php
            for($i=0; $i < $_GET['loop']; $i++){
                if($i % 2 == 0){
                    echo "<tr bgcolor = #008000><td>$i</td><td>$i</td><td>$i</td><td>$i</td><td>$i</td></tr>";
                }else{
                    echo "<tr bgcolor = #CCFFFF><td>$i</td><td>$i</td><td>$i</td><td>$i</td><td>$i</td></tr>"; // 奇数行の時の処理
                }
            }
            ?>
        </table>
    </body>
</html>
